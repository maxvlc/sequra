# frozen_string_literal: true

class Merchant < ApplicationRecord
  has_many :order
  has_many :disbursements
end
