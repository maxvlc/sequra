# frozen_string_literal: true

class Order < ApplicationRecord
  belongs_to :shopper

  scope :completed, -> { where('completed_at IS NOT NULL') }

  def calculate_disbursement
    return unless amount.positive?

    if amount < 50
      fee(1, amount)
    elsif amount >= 50 || amount <= 300
      fee(0.95, amount)
    else
      fee(0.85, amount)
    end
  end

  def fee(percentaje, amount)
    percentaje * amount.to_f / 100
  end
end
