# frozen_string_literal: true

class Disbursement < ApplicationRecord
  belongs_to :merchant

  class << self
    def by_week_and_merchant(week, merchant_id)
      where(merchant_id: merchant_id, week: week)
    end

    def by_week(week)
      where(week: week)
    end
  end
end
