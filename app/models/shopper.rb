# frozen_string_literal: true

class Shopper < ApplicationRecord
  has_many :orders
end
