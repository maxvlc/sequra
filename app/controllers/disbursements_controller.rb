# frozen_string_literal: true

class DisbursementsController < ApplicationController
  def disbursements
    @disbursements = if disbursement_params[:merchant_id].present? && disbursement_params[:week].present?
      Disbursement.by_week_and_merchant(disbursement_params[:week], disbursement_params[:merchant_id])
    else
      Disbursement.by_week(disbursement_params[:week])
    end

    if @disbursements.any?
      render json: { disbursements: @disbursements }, status: :ok
    else
      render plain: 'No data for that week'
    end
  end

  private

  def disbursement_params
    params.permit(:week, :merchant_id)
  end
end
