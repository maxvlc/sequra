# README
Max's technical test for Sequra

## Dependencies
Ruby 2.7.3
Rails 6.1.4.1
Postgresql
CSV gem
Haml
Byebug
rspec
FactoryBot
Rubygems

## Tasks explanation
* Create a rake task to create the data structures for Merchants, Shoppers and Orders into their respectives models, CSV files are into `/data` directory. This tasks should be executed in this order:
  `bin/rails db:import_merchants`
  `bin/rails db:import_shoppers`
  `bin/rails db:import_orders`

* Create a new task that must be executed as:
  `bin/rails calculate:disbursements(1, 5)`
  the first parameter is the `id` for the merchant and the second one is the `number of the week` .

  This task calculates the disbursements to be disbursed to each merchant. This task should be run as a crontab (for example) all Mondays, not has been done because application has not been deployed into any server.

* Create an endpoint to expose disbursements for a given merchant on a given week. If no merchant, all data for that week should be returned. Can be displayed for example on this URL:
  `http://localhost:3000/disbursements/3/1`
  displays data on week 1 from merchant with id 3.

  `http://localhost:3000/disbursements/3`
  displays data on week 3 from all merchants into the DDBB.

