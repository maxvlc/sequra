class CreateDisbursements < ActiveRecord::Migration[6.1]
  def change
    create_table :disbursements do |t|
      t.integer :merchant_id
      t.decimal :disbursement, precision: 12, scale: 2
      t.integer :week

      t.timestamps
    end
  end
end
