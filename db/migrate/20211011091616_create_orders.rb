class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.integer :merchant_id
      t.integer :shopper_id
      t.decimal :amount, precision: 12, scale: 2

      t.timestamp :created_at
      t.timestamp :completed_at
    end
  end
end
