# frozen_string_literal: true

Rails.application.routes.draw do
  get '/disbursements/:week(/:merchant_id)', to: 'disbursements#disbursements'
end
