# frozen_string_literal: true

require 'rails_helper'

describe Order do
  let(:shopper) { FactoryBot.create(:shopper) }
  let(:merchant) { FactoryBot.create(:merchant) }
  let(:order1) { FactoryBot.create(:order, merchant_id: merchant.id, shopper_id: shopper.id, amount: 1200 ) }
  let(:order2) { FactoryBot.create(:order, merchant_id: merchant.id, shopper_id: shopper.id, amount: 180 ) }
  let(:order3) { FactoryBot.create(:order, merchant_id: merchant.id, shopper_id: shopper.id, amount: 12 ) }

  context '#calculate_disbursement' do
    it 'for amount higher than 300 applies 0.85%' do
      expect(order1.calculate_disbursement).to eq 11.4
    end

    it 'for amount between 50 and 300 applies 0.95%' do
      expect(order2.calculate_disbursement).to eq 1.71
    end

    it 'for amount less than 50 applies 1%' do
      expect(order3.calculate_disbursement).to eq 0.12
    end
  end
end
