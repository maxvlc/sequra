# frozen_string_literal: true

require 'rails_helper'

Rails.application.load_tasks

describe DisbursementsController do
  let(:shopper) { FactoryBot.create(:shopper) }
  let(:merchant) { FactoryBot.create(:merchant) }
  let(:order) { FactoryBot.create(:order, merchant_id: merchant.id, shopper_id: shopper.id, amount: 1200, completed_at: '2018-01-03 02:32:49' ) }

  describe 'GET disbursements' do
      before do
        order.calculate_disbursement
        Rake::Task['calculate:disbursements'].invoke(1, 1)
      end

    context 'with valid params' do
      it 'with both correct params, returns a JSON with the data' do
        get :disbursements, params: { week: 1, merchant_id: merchant.id }

        parsed_json = JSON(response.body)
        expect(response.header['Content-Type']).to include('application/json')
        expect(parsed_json['disbursements'].first['merchant_id']).to eq merchant.id
        expect(parsed_json['disbursements'].first['week']).to eq 1
        expect(parsed_json['disbursements'].first['disbursement'].to_f).to eq order.calculate_disbursement
      end
    end

    context 'with no valid params' do
      it "with all the params no valids, returns a plain response with 'No data for that week' message" do
        get :disbursements, params: { week: 3, merchant_id: 7 }

        expect(response.body).to eq 'No data for that week'
      end

      it "with one no valid param, returns a plain response with 'No data for that week' message" do
        get :disbursements, params: { week: 7 }

        expect(response.body).to eq 'No data for that week'
      end
    end
  end
end
