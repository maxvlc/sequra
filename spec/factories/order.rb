# frozen_string_literal: true

FactoryBot.define do
  factory :order do
    merchant_id   { Random.rand(1..7) }
    shopper_id    { Random.rand(1..7) }
    amount        { 130.24 }
    created_at    { nil }
    completed_at  { nil }
  end
end
