# frozen_string_literal: true

FactoryBot.define do
  factory :merchant do
    name    { FFaker::Company.name }
    email   { FFaker::Internet.email }
    cif     { 'B611111112' }
  end
end
