# frozen_string_literal: true

FactoryBot.define do
  factory :shopper do
    name    { FFaker::Name.name }
    email   { FFaker::Internet.email }
    nif     { '411111112Z' }
  end
end
