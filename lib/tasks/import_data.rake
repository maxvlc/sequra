# frozen_string_literal: true

require 'csv'

namespace :db do
  task :import_merchants => :environment do
    CSV.foreach('data/merchants.csv', headers: :true) do |row|
      Merchant.create!(row.to_hash)
    end
  end

  task :import_shoppers => :environment do
    CSV.foreach('data/shoppers.csv', headers: :true) do |row|
      Shopper.create!(row.to_hash)
    end
  end

  task :import_orders => :environment do
    CSV.foreach('data/orders.csv', headers: :true) do |row|
      Order.create!(row.to_hash)
    end
  end
end
