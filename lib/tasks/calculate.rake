# frozen_string_literal: true

namespace :calculate do
  task :disbursements, [:week, :merchant_id] => :environment do |t, args|
    abort('Task only runs on Mondays') unless Time.now.monday?

    week_start = Date.commercial(2018, args[:week].to_i, 1)
    week_end = Date.commercial(2018, args[:week].to_i, 7)
    fee_on_week = 0

    Order.completed.where(merchant_id: args[:merchant_id].to_i, completed_at: week_start..week_end).each do |order|
      fee_on_week += order.calculate_disbursement
    end

    Disbursement.create(merchant_id: args[:merchant_id].to_i, disbursement: fee_on_week, week: args[:week].to_i)
  end
end
